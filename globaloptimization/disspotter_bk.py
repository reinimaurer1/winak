# winak.globaloptimization.disspotter
#
#    winak - python package for structure search and more in curvilinear coordinates
#    Copyright (C) 2016  Reinhard J. Maurer and Konstantin Krautgasser 
#    
#    This file is part of winak 
#        
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>#

from winak.curvilinear.InternalCoordinates import icSystem, Periodic_icSystem
from winak.curvilinear.InternalCoordinates import ValenceCoordinateGenerator as VCG
from winak.curvilinear.InternalCoordinates import PeriodicValenceCoordinateGenerator as PVCG
from winak.curvilinear.Coordinates import InternalCoordinates as IC
from ase.atoms import Atoms
import numpy as np
from ase.io import read

class DisSpotter:
    """
    Tells you if a molecule is dissociated or not.
    """
    def __init__(self, atoms):
        print 'initializing disspotter\n'
        if isinstance(atoms,Atoms):
            self.molecule=atoms
        else:
            self.molecule=read(atoms)
        
        self.visited=[False]*(len(self.molecule)+1)
        print 'self.visited', len(self.visited), self.visited, self.molecule
        if atoms.get_cell().any():
          print 'system is periodic'
          self.vcg=PVCG(self.molecule.get_chemical_symbols(),masses=self.molecule.get_masses(), cell=atoms.get_cell())
          self.iclist=self.vcg(self.molecule.get_positions().flatten())
          self.ic=Periodic_icSystem(self.iclist,len(self.molecule),masses=self.molecule.get_masses(),xyz=self.molecule.get_positions().flatten(), cell=atoms.get_cell(), expand=2)
        else:
          print 'system is not periodic'
          self.vcg=VCG(self.molecule.get_chemical_symbols(),masses=self.molecule.get_masses())
          self.ic=icSystem(self.iclist,len(self.molecule),masses=self.molecule.get_masses(),xyz=self.molecule.get_positions().flatten())

        self.stre=self.ic.getStretchBendTorsOop()[0][0]
        print 'stretches', len(self.stre), self.stre
        self.ics=self.ic.getStretchBendTorsOop()[1][:len(self.stre)]
        self.idx_c = set(np.array(self.ics).flatten())
        self.idx_c_noset = np.array(self.ics).flatten()
        print 'ics', len(self.ics), self.ics
        print 'connected atoms', len(self.idx_c), self.idx_c
        print 'connected atoms uncomp', len(self.idx_c_noset), self.idx_c_noset

    def numConnections(self,j):
        """ics: as in the main program bottom
           stre: list of stretches
           j: atom you want to find
        """
        print 'numConnections'
        found=0
        s=[]
        for i in self.stre:
            a=self.ics[i]
            if a[0]==j or a[1]==j:
                found+=1
                s.append(i)
        return found,s

    def fragment(self,j):
        """
        This might seem complicated, but it is actually not.
        """
        ret=0
        proceed=False
       # if j>=len(self.visited):
       #   ret+=1
       #   print 'atom '+str(j)+' is a periodic image, returning '+str(ret)
       #   return ret 
       # else:
       #   ret=0
        try:
          if not self.visited[j]:
            proceed=True
            self.visited[j]=True
        except:
              print 'atom '+str(j)+' is a periodic image, end here'
              proceed = True
              #return ret 
        print 'getting fragment, proceed', j, proceed
            
        if proceed:
        #if not self.visited[j]:
            ret+=1
            if j>=len(self.visited):
              print 'atom '+str(j)+' is a periodic image, returning '+str(ret)
              #ret+=1
            x=self.numConnections(j)
            #print 'looking at '+str(j)
            print str(j)+' has '+str(x[0])+' connections'
            if x[0]==1:
                print 'ending in atom '+str(j)+' and returning '+str(ret)
                return ret
            else:
                for l in x[1]:
                    tmp=self.ics[l][1]
                    if tmp==j:
                        tmp=self.ics[l][0]
                    print str(j)+' is connected to atom '+str(tmp)
                    ret+=self.fragment(tmp)
                    print 'back at atom '+str(j)
                    print 'ret is now '+str(ret)
            return ret
        else:
            print 'Atom '+str(j)+' already accounted for'
            return 0
        
    def get_fragments(self):
        """
        Returns all the fragments
        """
        print 'getting fragments'
        self.visited=[False]*(len(self.molecule)+1)
        self.visited[0]=True #whoever decided to make ics start at 1, go to hell
        ret=[]
        while True:
            print 'ret', ret    
            try:
                a=self.visited.index(False)
                tmp=[]
                tmp=self.get_fragment(a)
                ret.append(tmp)
            except ValueError:
                break
        return ret
        
        
    def get_fragment(self,j):
        """
        This might seem complicated, but it is actually not. 
        """
        ret=[]
        ## not working with periodic!!!
        #if j>=len(self.visited):
        #  print 'atom '+str(j)+' is a periodic image, ending here'
        #  ret+=1
        #  return ret 
        if not self.visited[j]:
            ret.append(j)
            self.visited[j]=True
            x=self.numConnections(j)
            #print 'looking at '+str(j)
            #print str(j)+' has '+str(x[0])+' connections'
            #if x[0]==1 and self.visited[self.ics[self.stre[x[1]]][0]] and self.visited[self.ics[self.stre[x[1]]][1]]:
            if x[0]==1 and self.visited[self.ics[self.stre[x[1][0]]][0]] and self.visited[self.ics[self.stre[x[1][0]]][1]]:
                #print 'ending in atom '+str(j)+' and returning '+str(ret)
                return ret
            else:
                for l in x[1]:
                    tmp=self.ics[l][1]
                    if tmp==j:
                        tmp=self.ics[l][0]
                    #print str(j)+' is connected to atom '+str(tmp)
                    r2=self.get_fragment(tmp)
                    for q in r2:
                        ret.append(q)
                    #print 'back at atom '+str(j)
                    #print 'ret is now '+str(ret)
            return ret
        else:
            #print 'Atom '+str(j)+' already accounted for'
            return []
        
    def spot_dis(self):
        """
        Returns true if dissociation happened
        """
        print '\nspotting diss\n'
        bla = self.fragment(1)
        print 'len molecule, len periodic, len fragment', len(self.molecule), len(self.idx_c), bla
        #return len(self.molecule)>bla
        return len(self.idx_c)>bla
        #return len(self.molecule)>self.fragment(1)#1 is arbitrary
