# winak.globaloptimization.disspotter
#
#    winak - python package for structure search and more in curvilinear coordinates
#    Copyright (C) 2016  Reinhard J. Maurer and Konstantin Krautgasser 
#    
#    This file is part of winak 
#        
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>#

from winak.curvilinear.InternalCoordinates import icSystem, Periodic_icSystem, Periodic_icSystem2
from winak.curvilinear.InternalCoordinates import ValenceCoordinateGenerator as VCG
from winak.curvilinear.InternalCoordinates import PeriodicValenceCoordinateGenerator as PVCG
from winak.curvilinear.InternalCoordinates import PeriodicValenceCoordinateGenerator2 as PVCG2
## with PVCG2 + Periodic_icSystem2 it works with expand=1
from winak.curvilinear.Coordinates import InternalCoordinates as IC
from ase.atoms import Atoms
import numpy as np
from ase.io import read

class DisSpotter:
    """
    Tells you if a molecule is dissociated or not.
    """
    def __init__(self, atoms):
        #print 'initializing disspotter\n'
        if isinstance(atoms,Atoms):
            self.molecule=atoms
        else:
            self.molecule=read(atoms)
        
        try:
          atoms.get_cell()
          #print 'system is periodic'
          self.vcg=PVCG2(self.molecule.get_chemical_symbols(),masses=self.molecule.get_masses(), cell=atoms.get_cell())
          self.iclist=self.vcg(self.molecule.get_positions().flatten())
          #self.ic=Periodic_icSystem(self.iclist,len(self.molecule),masses=self.molecule.get_masses(),xyz=self.molecule.get_positions().flatten(), cell=atoms.get_cell(), expand=1)
          #self.ic=Periodic_icSystem(self.iclist,len(self.molecule),masses=self.molecule.get_masses(),xyz=self.molecule.get_positions().flatten(), cell=atoms.get_cell(), expand=2)
          self.ic=Periodic_icSystem2(self.iclist,len(self.molecule),masses=self.molecule.get_masses(),xyz=self.molecule.get_positions().flatten(), cell=atoms.get_cell(), expand=1)

        except:
          #print 'system is not periodic'
          self.vcg=VCG(self.molecule.get_chemical_symbols(),masses=self.molecule.get_masses())
          self.iclist=self.vcg(self.molecule.get_positions().flatten())
          self.ic=icSystem(self.iclist,len(self.molecule),masses=self.molecule.get_masses(),xyz=self.molecule.get_positions().flatten())

        self.stre=self.ic.getStretchBendTorsOop()[0][0]
        #print 'stretches', len(self.stre), self.stre
        try:
          self.ics=self.ic.getStretchBendTorsOop()[1] #[:len(self.stre)]
          self.ics=np.array(self.ic.getStretchBendTorsOop()[1][:len(self.stre)])
          shape=self.ics.shape
          #print 'ics', self.ics, self.ics.shape
        except:
          print 'wtf'
        self.idx_c = sorted(list(set(np.array(self.ics).flatten())))
        #print 'connected atoms', len(self.idx_c), self.idx_c
        
        #remap
        self.ics=self.ics.flatten()
        #print self.ics
        for n,c in enumerate(self.idx_c):
          #print 'c, n, n+1', c, n, n+1
          if c==n+1:
            pass
          else:
            self.idx_c[n] = n+1
            replace = np.where(self.ics==c)[0]
            #print 'replace', c, 'with', n+1, 'in', replace
            #print 'ics', self.ics
            self.ics[replace] = n+1 
            #print 'ics', self.ics
        self.ics = np.reshape( self.ics, (len(self.stre), 2 ))
        #print 'connected atoms', len(self.idx_c), self.idx_c
        #print 'ics', self.ics, self.ics.shape
        self.visited=[False]*(len(self.idx_c)+1)
        #print 'self.visited', len(self.visited), self.visited, self.molecule
          
    def numConnections(self,j):
        """ics: as in the main program bottom
           stre: list of stretches
           j: atom you want to find
        """
        #print 'numConnections'
        found=0
        s=[]
        for n,i in enumerate(self.stre):
            #print 'idx, atom', n, i
            a=self.ics[i]
            if a[0]==j or a[1]==j:
                found+=1
                s.append(i)
        return found,s

    def fragment(self,j):
        """
        This might seem complicated, but it is actually not.
        """
        ret=0

        if not self.visited[j]:
            ret+=1
            self.visited[j]=True
            x=self.numConnections(j)
            #print 'looking at '+str(j)
            #print str(j)+' has '+str(x[0])+' connections'
            if x[0]==1:
                #print 'ending in atom '+str(j)+' and returning '+str(ret)
                return ret
            else:
                for l in x[1]:
                    tmp=self.ics[l][1]
                    if tmp==j:
                        tmp=self.ics[l][0]
                    #print str(j)+' is connected to atom '+str(tmp)
                    ret+=self.fragment(tmp)
                    #print 'back at atom '+str(j)
                    #print 'ret is now '+str(ret)
            return ret
        else:
            #print 'Atom '+str(j)+' already accounted for'
            return 0
        
    def get_fragments(self):
        """
        Returns all the fragments
        """
        #print 'getting fragments'
        self.visited=[False]*(len(self.idx_c)+1)
        self.visited[0]=True #whoever decided to make ics start at 1, go to hell
        ret=[]
        while True:
            #print 'ret', ret    
            try:
                a=self.visited.index(False)
                tmp=[]
                tmp=self.get_fragment(a)
                ret.append(tmp)
            except ValueError:
                break
        return ret
        
        
    def get_fragment(self,j):
        """
        This might seem complicated, but it is actually not. 
        """
        ret=[]
        ## not working with periodic!!!
        if not self.visited[j]:
            ret.append(j)
            self.visited[j]=True
            x=self.numConnections(j)
            #print 'looking at '+str(j)
            #print str(j)+' has '+str(x[0])+' connections'
            #if x[0]==1 and self.visited[self.ics[self.stre[x[1]]][0]] and self.visited[self.ics[self.stre[x[1]]][1]]:
            if x[0]==1 and self.visited[self.ics[self.stre[x[1][0]]][0]] and self.visited[self.ics[self.stre[x[1][0]]][1]]:
                #print 'ending in atom '+str(j)+' and returning '+str(ret)
                return ret
            else:
                for l in x[1]:
                    tmp=self.ics[l][1]
                    if tmp==j:
                        tmp=self.ics[l][0]
                    #print str(j)+' is connected to atom '+str(tmp)
                    r2=self.get_fragment(tmp)
                    for q in r2:
                        ret.append(q)
                    #print 'back at atom '+str(j)
                    #print 'ret is now '+str(ret)
            return ret
        else:
            #print 'Atom '+str(j)+' already accounted for'
            return []
        
    def spot_dis(self):
        """
        Returns true if dissociation happened
        """
        #print '\nspotting diss\n'
        bla = self.fragment(1)
        #print 'len molecule, len periodic, len fragment', len(self.molecule), len(self.idx_c), bla
        #print self.visited
        #return len(self.molecule)>bla
        return len(self.idx_c)>bla
        #return len(self.molecule)>self.fragment(1)#1 is arbitrary
