from winak.globaloptimization.disspotter_new import *
#from winak.globaloptimization.disspotter import *
from ase.visualize import view
from ase.io import read

## this is a mess, fix


#d=DisSpotter(file='diss.xyz')
d=DisSpotter('diss.xyz')

a=''
print 'spot_dis', d.spot_dis()
if not d.spot_dis():
    ## spot dis = true
    ## not spot dis = false
    ## a = ''
    a='not '

print 'Molecule 1 is '+a+'dissociated!\n'

frags = d.get_fragments()
print frags

atoms=read('diss.xyz')
for f in frags:
  idx=np.array(f)
  view(atoms[idx-1])


#d=DisSpotter(file='notdiss.xyz')
d=DisSpotter('notdiss.xyz')
val = d.spot_dis()

a=''
print 'spot_dis', val
print 'not spot_dis', not val
print 'spot dis false', val==False
#if not d.spot_dis():
#if val==False:
if not val:
    # spot dis = false
    # not spot dis = true
    # a = 'not'
    print 'hey'
    a='not '

print 'Molecule 2 is '+a+'dissociated!\n'
