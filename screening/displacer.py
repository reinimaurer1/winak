# winak.screening.displacer
#
#    winak - python package for structure search and more in curvilinear coordinates
#    Copyright (C) 2016  Reinhard J. Maurer and Konstantin Krautgasser 
#    
#    This file is part of winak 
#        
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>#

from abc import ABCMeta, abstractmethod

### CP - ugly hack to deal with ASE backwards compatibility; this is temporary
try:
    from ase.build.tools import sort
except:
    from ase.utils.geometry import sort
try:
    from ase.neighborlist import NeighborList
except:
    from ase.calculators.neighborlist import NeighborList
###
from ase.visualize import view
from ase.atom import Atom
import numpy as np
from winak.curvilinear.Coordinates import DelocalizedCoordinates as DC
from winak.curvilinear.Coordinates import PeriodicCoordinates as PC
from winak.globaloptimization.delocalizer import *
from winak.curvilinear.Coordinates import CompleteDelocalizedCoordinates as CDC
from winak.curvilinear.InternalCoordinates import ValenceCoordinateGenerator as VCG
from winak.curvilinear.InternalCoordinates import icSystem
from winak.globaloptimization.disspotter import DisSpotter
from winak.screening.composition import *
import os

class Displacer:
    """This class performs a step in any way you see fit. It is also 
    in charge of logging."""
    __metaclass__ = ABCMeta

    def __init__(self):
        """subclasses must call this method."""
        ## consider moving global parameters (verbose, seeded) here
        pass
        
    @abstractmethod
    def displace(self,tmp):
        """subclasses must implement this method. Has to return a displace 
        ase.atoms object"""
        pass
    
    @abstractmethod
    def print_params(self):
        """subclasses must implement this method. Has to return a string containing
        all the important parameters"""
        pass
    

class MultiDI(Displacer):
    def __init__(self,stepwidth,numdelocmodes=1,constrain=False,adsorbate=None,cell_scale=[1.0,1.0,0.05],adjust_cm=True,periodic=False,dense=True,loghax=False, verbose=False):
        """cell_scale: for translations; scales translations, so everything
        stays in the unit cell. The z component should be set to something small,
        like 0.05. 
        numdelocmodes: how many delocalized internals should be combined; from
        0 to <1 it will interpret it as a percentage (i.e. 0.25=25%), 1 or any
        number larger than 1 will be interpreted as a certain number of modes 
        (i.e. 1=1 deloc mode, 15=15 delocmodes etc.)"""
        Displacer.__init__(self)
        if adsorbate is None:
            self.ads=False
        else:
            self.adsorbate=adsorbate
            self.ads=True
        self.cell_scale=cell_scale
        self.constrain=constrain
        self.dense=dense
        self.adjust_cm=adjust_cm
        self.stepwidth=stepwidth
        self.numdelmodes=np.abs(numdelocmodes)
        self.lh=loghax
        self.periodic=periodic
        self.verbose=verbose

    def displace(self, tmp, seed):
        atms=tmp.copy()
        if self.ads:
            ads=tmp[self.adsorbate[0]:self.adsorbate[1]].copy()
            tmp1=tmp[:self.adsorbate[0]].copy()
            tmp2=tmp[self.adsorbate[1]:].copy()
            if len(tmp1)==0:
                surf=tmp2
            elif len(tmp2)==0:
                surf=tmp1
            else:
                surf=tmp1
                for i in tmp2:
                    surf.append(i)
        else:
            ads=tmp.copy()
        d=DisSpotter(ads)
        a=d.get_fragments()
        #tmp.write('overall.xyz')
        o=1
        if self.lh:
            while True:
                try:
                    os.stat(str(o))
                    o+=1
                except:
                    os.mkdir(str(o))       
                    break
            tmp.write(os.path.join(str(o),'pre_overall.xyz'))
        for i in a:
            i=np.asarray(i)-1
            if self.ads:
                tt=surf.copy()
                for j in ads[i]:
                    tt.append(j)
                adstmp=[len(surf),len(tt)]
                ads0=len(surf)
            else:
                tt=tmp[i]
                adstmp=None
                ads0=0
            
            di=DI(self.stepwidth,numdelocmodes=self.numdelmodes,constrain=self.constrain,adsorbate=adstmp,cell_scale=self.cell_scale,adjust_cm=self.adjust_cm,periodic=self.periodic,dense=self.dense)
            if self.lh:
                tt.write(os.path.join(str(o),'pre_'+str(i[0])+'.xyz'))
            tt=di.displace(tt)
            if self.lh:
                tt.write(os.path.join(str(o),'post_'+str(i[0])+'.xyz'))
            #tt.write(str(i[0])+'.xyz')
            k=0
            for j in i:
                atms[ads0+j].set('position',tt[ads0+k].get('position'))
                k+=1
        if self.lh:
            atms.write(os.path.join(str(o),'post_overall.xyz'))
        return atms
    
    def print_params(self):
        if self.ads:
            ads=', %d:%d is displaced'%(self.adsorbate[0],self.adsorbate[1])
        else:
            ads=''
        if self.constrain:
            cc=''
        else:
            cc=' not'
        return '%s: stepwidth=%f%s, stretches are%s constrained'%(self.__class__.__name__,self.stepwidth,ads,cc)
        
class DI(Displacer):
    def __init__(self, stepwidth, numdelocmodes=1, constrain=False, adsorbate=None, cell_scale=[1.0,1.0,0.05], adjust_cm=True, periodic=False, dense=False, weighted=False, thresholds=[0.15,170,160], seeded=False, verbose=False,doublesurf=False, per_version=2, expand=2):
        """cell_scale: for translations; scales translations, so everything
        stays in the unit cell. The z component should be set to something small,
        like 0.05. 
        numdelocmodes: how many delocalized internals should be combined; from
        0 to <1 it will interpret it as a percentage (i.e. 0.25=25%), 1 or any
        number larger than 1 will be interpreted as a certain number of modes 
        (i.e. 1=1 deloc mode, 15=15 delocmodes etc.)"""
        Displacer.__init__(self)
        self.verbose=True #verbose no idea why turns out false
        if adsorbate is None:
            self.ads=False
        else:
            self.adsorbate=adsorbate
            self.ads=True
            self.cell_scale=cell_scale
        self.constrain=constrain
        self.adjust_cm=adjust_cm
        self.stepwidth=stepwidth
        self.numdelmodes=np.abs(numdelocmodes)
        self.dense=dense
        self.weighted=weighted
        self.periodic=periodic
        self.thresholds=thresholds
        if self.verbose: print self.thresholds
        self.seeded=seeded
        if self.verbose: print 'seeded: ', self.seeded
        self.seed=12345
        self.doublesurf = doublesurf
        self.per_version = per_version
        self.expand = expand
        print 'verbose', self.verbose
        if self.verbose: print 'DI init OK'
        if self.verbose: print 'DI vars', vars(self)
         
    def get_vectors(self,atoms):
        if self.periodic:
            if self.verbose: print 'displacer get_vectors: calling delocalizer periodic' #, vars(deloc)
            deloc=Delocalizer(atoms,periodic=True,
                    expand=self.expand,
                    per_version=self.per_version,
                    dense=self.dense,
                    weighted=self.weighted,
                    threshold=self.thresholds[0],
                    bendThreshold=self.thresholds[1],
                    torsionThreshold=self.thresholds[2])
            if self.verbose: print 'displacer get_vectors: calling coords periodic' #, vars(deloc)
            coords=PC(deloc.x_ref.flatten(), deloc.masses, atoms=deloc.atoms, ic=deloc.ic, Li=deloc.u)
            if self.verbose: print 'displacer get_vectors: done coords periodic' #, vars(deloc)
        else:
            if self.verbose: print 'displacer get_vectors: calling delocalizer non periodic' #, vars(deloc)
            deloc=Delocalizer(atoms,dense=self.dense,
                    weighted=self.weighted,
                    threshold=self.thresholds[0],
                    bendThreshold=self.thresholds[1],
                    torsionThreshold=self.thresholds[2])
            tu=deloc.u
            if self.constrain:
                if self.verbose: print 'displacer get_vectors: applying constr'
                e = deloc.constrainStretches()
                deloc.constrain(e)
                tu=deloc.u2
                if self.verbose: print 'displacer get_vectors: applied constr'
            if not self.ads:
                if self.verbose: print 'displacer get_vectors: coords non periodic' #, vars(deloc)
                coords=DC(deloc.x_ref.flatten(), deloc.masses, atoms=deloc.atoms, ic=deloc.ic, u=tu)
                if self.verbose: print 'displacer get_vectors: done coords non periodic' #, vars(deloc)
            else:
                cell = atoms.get_cell()*self.cell_scale
                if self.verbose: print 'displacer get_vectors: complete coords non periodic' #, vars(deloc)
                coords=CDC(deloc.x_ref.flatten(), deloc.masses, unit=1.0, atoms=deloc.atoms, ic=deloc.ic, u=tu,cell=cell)
                if self.verbose: print 'displacer get_vectors: done complete non periodic' #, vars(deloc)
        
        return coords.get_vectors() 
    
    def displace(self, tmp, seed):
        """No DIS for atoms! Atoms get Cart movements. If on surf, along cell"""
        if self.verbose: print 'entering displacer method displace'
        atms=tmp.copy()
        if self.adjust_cm:
            try:
                #print 'adjusting COM before displacement, surf'
                #print tmp
                cell = tmp.get_cell()
                #print 'cell', cell
                cmt = np.array([cell[0][0], cell[1][1], cell[2][2]])/2
            except:
                #print 'adjusting COM before displacement, clu'
                cmt = tmp.get_center_of_mass()  
            cm = tmp.get_center_of_mass()
            atms.translate(cmt - cm)
        disp=np.zeros((len(tmp),3))
        ro=tmp.get_positions()
        if self.ads:
            #print 'adsorbate'
            # rearrange , not necessary if well defined
            ads=Atoms([atom for atom in atms if atom.tag==100])         ### separate
            #print 'ads'
            slab=Atoms([atom for atom in atms if not atom.tag==100],pbc=atms.pbc,cell=atms.cell,constraint=atms.constraints)
            #slab=Atoms([atom for atom in atms if not atom.tag==100],pbc=atms.pbc,cell=atms.cell)
            #print 'slab'
            atms = slab.extend(ads)
            #print 'rearranged'
            adsidx=[atom.index for atom in atms if atom.tag==100]         ### separate, indices
            #print 'ads indices', adsidx
            self.adsorbate=[adsidx[0],adsidx[-1]+1]
            #print self.adsorbate
            #tmp=tmp[self.adsorbate[0]:self.adsorbate[1]].copy()
            tmp=atms[self.adsorbate[0]:self.adsorbate[1]].copy()
            ads1=self.adsorbate[0]
            ads2=self.adsorbate[1]
        else:
            #print 'no adsorbate'
            ads1=0
            ads2=len(tmp)

        if len(tmp)>1:
            if self.numdelmodes<1:
                if self.ads:
                    nummodes=int(np.round(self.numdelmodes*len(tmp)*3))#3N dis in adsorbates
                    if self.verbose: print 'number of modes:', nummodes, 'out of', len(tmp)*3
                else:
                    nummodes=int(np.round(self.numdelmodes*(len(tmp)*3-6)))#3N-6 dis in gas phase
                    if self.verbose: print 'number of modes:', nummodes, 'out of', len(tmp)*3-6
            else:
                nummodes=int(np.round(self.numdelmodes))#round and int just for you trolls out there 
                if self.verbose: print 'number of modes:', nummodes, 'out of', len(tmp)*3
            if self.verbose: print 'displacer, displace: getting vectors'
            if self.verbose: print 'displacer, number of atoms', len(tmp)
            vectors=self.get_vectors(tmp)
            if self.verbose: print 'displacer, displace: got vectors'
            numvec=len(vectors)
            start=0
            if self.constrain:
                mm=tmp.get_masses()
                x0=tmp.get_positions().flatten()
                if self.verbose: print 'displacer, displace: constrained, calling VCG'
                vv=VCG(tmp.get_chemical_symbols(),masses=mm)
                if self.verbose: print 'displace, displace: constrained, called VCG'
                start=len(icSystem(vv(x0),len(tmp), masses=mm,xyz=x0).getStretchBendTorsOop()[0][0])
            if nummodes>numvec-start:
                nummodes=numvec-start
            if self.seeded:
                if self.verbose: print 'displacer, displace: seed DI choice', seed
                np.random.seed(seed)
            w=np.random.choice(range(start,numvec),size=nummodes,replace=False)
            if self.verbose: print 'displacer, displace: displacing'
            for i in w:
                if self.seeded:
                    if self.verbose: print 'displacer, displace: seed displacement', seed
                    np.random.seed(seed)
                disp[ads1:ads2,:3]+=vectors[i]*np.random.uniform(-1.,1.) #this is important if there is an adsorbate.
            if self.verbose: print 'displacer, displace: calculated displacement'
            disp/=np.max(np.abs(disp))
            rn=ro+self.stepwidth*disp
            if self.verbose: print 'displacer, displace: applied displacement'
            
            if self.doublesurf:
                print 'please implement me'
                
            if self.adjust_cm:
                try:
                    cell = slab.get_cell()
                    cmt = np.array([cell[0][0], cell[1][1], cell[2][2]])/2
                except:
                    cmt = tmp.get_center_of_mass()  
                
            atms.set_positions(rn)
            
            if self.adjust_cm:         
                #print 'adjusting COM post displacement'
                cm = atms.get_center_of_mass()
                atms.translate(cmt - cm)
                #print 'cmt, cm, newcm', cmt, cm, atms.get_center_of_mass()
        elif self.ads:
            cc=Celltrans(self.stepwidth,self.adsorbate,self.cell_scale,self.adjust_cm)
            atms=cc.displace(atms)
        else:
            cc=Cartesian(self.stepwidth,self.adsorbate,self.adjust_cm)
            atms=cc.displace(atms)
        return atms
    
    def print_params(self):
        if self.ads:
            ads=', %d:%d is displaced'%(self.adsorbate[0],self.adsorbate[1])
        else:
            ads=''
        if self.constrain:
            cc=''
        else:
            cc=' not'
        return '%s: stepwidth=%f, numdelocmodes=%f%s, stretches are%s constrained'%(self.__class__.__name__,self.stepwidth,self.numdelmodes,ads,cc)
    
class Celltrans(Displacer):
    """displaces along cell vectors"""
    def __init__(self,stepwidth,adsorbate,cell_scale=[1.,1.,1.],adjust_cm=True, verbose=False, seeded=False):
        Displacer.__init__(self)
        self.stepwidth=stepwidth
        self.adsorbate=adsorbate
        self.cell_scale=cell_scale
        self.adjust_cm=adjust_cm
        self.verbose=verbose
        self.seeded=seeded
        
    def displace(self, tmp, seed):
        ro=tmp.get_positions()
        disp=np.zeros((len(ro),3))
        c=tmp.get_cell()*self.cell_scale
        if self.seeded:
            np.random.seed(seed)
        disp[self.adsorbate[0]:self.adsorbate[1],:3] = np.dot(c,np.random.uniform(-1., 1., 3))
        rn = ro + self.stepwidth * disp
        if self.adjust_cm:
            ### this can be done once and for all with self.com or something like that TODO
            try:
                cell = slab.get_cell()
                cmt = np.array([cell[0][0], cell[1][1], cell[2][2]])/2
            except:
                cmt = tmp.get_center_of_mass()  
        tmp.set_positions(rn)
        if self.adjust_cm:
            cm = tmp.get_center_of_mass()
            tmp.translate(cmt - cm)  
        return tmp
    
    def print_params(self):
        ads=', %d:%d is displaced'%(self.adsorbate[0],self.adsorbate[1])
        return '%s: stepwidth=%f%s'%(self.__class__.__name__,self.stepwidth,ads)
    
class Cartesian(Displacer):
    def __init__(self,stepwidth,adsorbate=None,adjust_cm=True, verbose=False, seeded=False):
        Displacer.__init__(self)
        if adsorbate is None:
            self.ads = False
        else:
            self.adsorbate = adsorbate
            self.ads = True
        self.adjust_cm = adjust_cm
        self.stepwidth = stepwidth
        self.verbose = verbose
        self.seeded = seeded

    def displace(self, tmp, seed):
        """Move atoms by a random step."""
        ro=tmp.get_positions()
        disp=np.zeros((len(ro),3))
        if self.seeded:
            np.random.seed(seed)
        if self.ads:
            disp[self.adsorbate[0]:self.adsorbate[1],:3] = np.random.uniform(-1., 1., (self.adsorbate[1]-self.adsorbate[0], 3))
        else:
            disp[:,:3] = np.random.uniform(-1., 1., (len(tmp), 3))
        rn = ro + self.stepwidth * disp
        if self.adjust_cm:
            try:
                cell = slab.get_cell()
                cmt = np.array([cell[0][0], cell[1][1], cell[2][2]])/2
            except:
                cmt = tmp.get_center_of_mass()  
        tmp.set_positions(rn) 
        if self.adjust_cm:
            cm = tmp.get_center_of_mass()
            tmp.translate(cmt - cm)   
        return tmp
    
    def print_params(self):
        if self.ads:
            ads=', %d:%d is displaced'%(self.adsorbate[0],self.adsorbate[1])
        else:
            ads=''
        return '%s: stepwidth=%f%s'%(self.__class__.__name__,self.stepwidth,ads)

class Remove(Displacer):
    def __init__(self,prob=0.5,adsorbate=None,adjust_cm=True,atm=None, verbose=False, seeded=False, doublesurf=False):
        '''Work in progress: here adsorbate must be an integer. Atoms tagged with it belong to the adsorbate'''
        Displacer.__init__(self)
        #print 'init remove'
        if adsorbate is None:
            self.ads=False
        else:
            self.adsorbate=adsorbate ## INT!!
            self.ads=True
        self.adjust_cm=adjust_cm
        self.prob=prob
        self.atm=atm
        self.verbose=verbose
        self.seeded=seeded
        self.doublesurf = doublesurf

    def pop_atom(self, atoms, slab=None):
        #print 'attempting pop atom'
        if self.ads:
            ads=Atoms([atom for atom in atoms if atom.tag==self.adsorbate])         ### separate
            tmp=ads.copy()
            slab=Atoms([atom for atom in atoms if not atom.tag==self.adsorbate],pbc=atoms.pbc,cell=atoms.cell,constraint=atoms.constraints)
        else:
            tmp=atoms.copy()
        if self.seeded:
            np.random.seed(seed)
        if self.atm is None:
            idx=np.random.choice(tmp).index                                     ### pop atom from ads
        else:
            idx=np.random.choice([atom.index for atom in tmp if atom.symbol==self.atm])
        nspecies=len(Stoichiometry().get(tmp).keys())          ### to avoid disappearing species
        #print 'selected pop atom'
        #print 'double', self.doublesurf
        if self.doublesurf:
            #print 'popping twin atom to', tmp[idx]
            z=slab.get_cell()[2][2]-tmp[idx].z
            #print 'got z'
            twin = [atom for atom in tmp if np.abs(atom.x - tmp[idx].x)<0.1 and np.abs(atom.y-tmp[idx].y)<0.1 and np.abs(atom.z-z)<0.1][0] ##list
            #print 'twin', twin
            twinidx=twin.index
            tmp.pop(twinidx)                                                              ### pop atom from ads
        #print 'pop atom', idx
        tmp.pop(idx)                                                              ### pop atom from ads
        #print 'sorting'
        tmp=sort(tmp)
        return tmp, nspecies, slab          ## will put back together in displace subclass
    
    def displace(self, tmp, seed, ok=False):
        """Randomly remove atom with probability=prob"""
        #CP sorting displaced atoms (or adsorbate only) is needed otherwise ase refuses to write trajectories if the composition is the same but the ordering of atoms differs, i.e. after insertion+removal+insertion. Sorting every time the composition changes should be enough
        # check if this is still needed with newer ase
        
        if self.adjust_cm:
            try:
                cell = slab.get_cell()
                cmt = np.array([cell[0][0], cell[1][1], cell[2][2]])/2
            except:
                cmt = tmp.get_center_of_mass()  
        
        if self.seeded:
            np.random.seed(seed)
        
        if np.random.random() < self.prob:
            tries=0
            while not ok:
                #pippo,nspecies,slab=self.pop_atom(tmp.copy())
                pippo,nspecies,slab=self.pop_atom(tmp.copy())
                try:
                    ok=(len(Stoichiometry().get(pippo).keys())==nspecies) ## check if atomic species disappears; also includes the case of running out of atoms completely 
                except:                                                     ### though maybe that should be treated separately and break, to avoid doing this 10 times
                    print 'atomic species disappeared'
                tries+=1
                #print tries
                if tries>3:
                    return None
                    break
            if self.ads:
                tmp=slab.extend(pippo)                                                ### put back together
            else:
                tmp=pippo
        if self.adjust_cm:
            cm = tmp.get_center_of_mass()
            tmp.translate(cmt - cm)   
        return tmp
    
    def print_params(self):
        if self.ads:
            ads=', %d: is displaced'%(self.adsorbate)
        else:
            ads=''
        return '%s: probability=%f%s'%(self.__class__.__name__,self.prob,ads)

class Insert(Displacer):
    def __init__(self,prob=0.5,adsorbate=None,adjust_cm=True,sys_type='cluster',mode='nn',atm=None, verbose=False, seeded=False, doublesurf=False, maxcontent=100000):
        '''Work in progress: here adsorbate must be an integer. Atoms tagged with it belong to the adsorbate'''
        Displacer.__init__(self)
        self.verbose=verbose
        if adsorbate is None:
            self.ads=False
        else:
            self.adsorbate=adsorbate
            self.ads=True
        self.adjust_cm=adjust_cm
        self.prob=prob
        self.sys_type=sys_type
        self.mode=mode
        self.atm=atm
        self.seeded=seeded
        self.doublesurf=doublesurf
        self.seeded=seeded
        self.maxcontent=maxcontent

    def nn(self,tmp,cutoff=1.5):
        """Picks a random atom and its nearest neighbor and puts new atom somewhere in between"""
        com=tmp.get_center_of_mass()
        tmp.translate(-com)
        cutoffs=np.full(len(tmp),cutoff) ## TODO:read from covalent radii data 
        nl=NeighborList(cutoffs, bothways=True)
        nl.build(tmp)
        if self.seeded:
            np.random.seed(seed)
        pippo=np.random.choice(tmp).index
        pluto=nl.get_neighbors(pippo)[0][1] ## index of nearest neighbour. Mind: [0][0] is the atom itself
        #new=(tmp.positions[pippo]+tmp.positions[pluto])/2+[0.,0.,cutoff] ## push a bit up along z, for adsorbates TODO push away from COM rather than up
        new=(tmp.positions[pippo]+tmp.positions[pluto])/2
        new=new*(1+cutoff/np.linalg.norm(new))
        return com+new

    def randomize(self,tmp):
        """Generates coordinates of new atom on the surface of the sphere enclosing the current structure. Works well with clusters.
        """
        com=tmp.get_center_of_mass()
        tmp.translate(-com)
        R=max(np.linalg.norm(tmp.get_positions(), axis=1))
        if self.seeded:
            np.random.seed(seed)
        R = np.random.uniform(0,R) ## should be crap, use for performance evaluation (as a bad example)
        if self.seeded:
            np.random.seed(seed)
        u = np.random.uniform(0,1)
        if self.seeded:
            np.random.seed(seed)
        v = np.random.uniform(0,1)
        theta = 2*np.pi*u
        phi = np.arccos(2*v-1)
        x = R*np.cos(theta)*np.cos(phi)
        y = R*np.cos(theta)*np.sin(phi)
        z = R*np.sin(theta)
        return com+(x,y,z) #### find a smart way to do this for either cluster or adsorbate
    
    def randomize_surf(self,tmp):
        """Generates coordinates of new atom on the surface of the sphere enclosing the current structure. Works well with clusters.
        """
        com=tmp.get_center_of_mass()
        tmp.translate(-com)
        R=max(np.linalg.norm(tmp.get_positions(), axis=1))
        #R = (1+1/R)*R ## should alleviate overlaps
        if self.seeded:
            np.random.seed(seed)
        u = np.random.uniform(0,1)
        if self.seeded:
            np.random.seed(seed)
        v = np.random.uniform(0,1)
        theta = 2*np.pi*u
        phi = np.arccos(2*v-1)
        x = R*np.cos(theta)*np.cos(phi)
        y = R*np.cos(theta)*np.sin(phi)
        z = R*np.sin(theta)
        return com+(x,y,z) #### find a smart way to do this for either cluster or adsorbate

    def random_duplicate(self,tmp):
        """pick a random atom and duplicate it, shifting it a bit farther from the COM --- not working atm"""
        tmp.translate(-tmp.get_center_of_mass())
        newatom=tmp[-1]#np.random.choice(tmp)
        newatom.position*=1.5
        return newatom.position

    def displace(self, tmp, seed):
        """Randomly insert atom with probability=prob. Atom type is chosen randomly from the composition."""
        if self.adjust_cm:
            try:
                cell = slab.get_cell()
                cmt = np.array([cell[0][0], cell[1][1], cell[2][2]])/2
            except:
                cmt = tmp.get_center_of_mass()  
        if self.seeded:
            np.random.seed(seed)
        if np.random.random() < self.prob:
            if self.ads:
                ads=Atoms([atom for atom in tmp if atom.tag==self.adsorbate])         ### separate
                slab=Atoms([atom for atom in tmp if not atom.tag==self.adsorbate],pbc=tmp.pbc,cell=tmp.cell,constraint=tmp.constraints)
                tmp=ads.copy()
            if self.atm is None:
                if self.seeded:
                    np.random.seed(seed)
                atm=np.random.choice(Stoichiometry().get(tmp).keys())
            else:
                atm = self.atm
            
            if self.mode=='nn': 
                newpos=self.nn(tmp.copy())
            elif self.mode=='rsphere':
                newpos=self.randomize(tmp.copy())
            elif self.mode=='rsurf':
                newpos=self.randomize_surf(tmp.copy())
            else:
                raise ValueError('Invalid insertion mode')
            if self.ads:
                tmp.append(Atom(atm,newpos,tag=100))
                if self.doublesurf:
                    z=slab.get_cell()[2][2]-newpos[2]
                    newpos[2]=z
                    tmp.append(Atom(atm,newpos,tag=100))
                tmp=slab.extend(sort(tmp))                                                ### put back together
            else:
                tmp.append(Atom(atm,newpos))
                tmp=sort(tmp)

        if self.adjust_cm:
            cm = tmp.get_center_of_mass()
            tmp.translate(cmt - cm)   
        content = Stoichiometry().get(tmp)[atm]
        #print 'content of', atm, content 
        ## Stoichiometry().get(tmp).keys())          ### to avoid disappearing species
        if content > self.maxcontent:
            print 'too much of atomic species'
            tmp = None
        return tmp
    
    def print_params(self):
        if self.ads:
            ads=', %d: is displaced'%(self.adsorbate)
        else:
            ads=''
        return '%s: probability=%f%s'%(self.__class__.__name__,self.prob,ads)

class GC(Displacer):
    """Grand Canonical moves: insert or remove particle with probability prob, otherwise displace in DICs"""
    def __init__(self,prob=0.5,stepwidth=1.0,numdelocmodes=1,constrain=False,adsorbate=None,cell_scale=[1.0,1.0,0.05],adjust_cm=True,periodic=False,bias=0.5,ins_mode='nn',atm=None, verbose=False, doublesurf=False, seeded=False, maxcontent=10000, dense=False, weighted=False, thresholds=[0.15,170,160], per_version=2, expand=1):
        Displacer.__init__(self)
        self.verbose=verbose
        if adsorbate is None:
            self.ads=False
            self.adsorbate=adsorbate
        else:
            self.adsorbate=adsorbate
            self.ads=True
        self.prob=prob
        self.constrain=constrain
        self.stepwidth=stepwidth
        self.numdelmodes=np.abs(numdelocmodes)
        self.periodic=periodic
        self.adjust_cm=adjust_cm
        self.bias=bias ## bias towards either insertion or removal (I/R ratio)
        self.ins_mode=ins_mode
        self.atm=atm
        self.cell_scale=cell_scale
        self.doublesurf=doublesurf
        self.seeded=seeded ## check inheritance here
        self.maxcontent=maxcontent
        self.dense=dense
        self.weighted=weighted
        self.thresholds = thresholds
        self.per_version = per_version
        self.expand = expand
        if self.verbose: print 'GC vars', vars(self)

    def displace(self, tmp, seed):
        """Randomly insert atom with probability=prob"""
        if self.adjust_cm:
            try:
                cell = slab.get_cell()
                cmt = np.array([cell[0][0], cell[1][1], cell[2][2]])/2
            except:
                cmt = tmp.get_center_of_mass()  
        if self.seeded:
            np.random.seed(seed)
        if np.random.random() < self.prob:
            if self.seeded:
                np.random.seed(seed)
            if np.random.random() < self.bias:  ##toss coin insert or remove or bias towards either
                disp=Insert(prob=1,adsorbate=self.adsorbate,adjust_cm=self.adjust_cm,mode=self.ins_mode,atm=self.atm, doublesurf=self.doublesurf, seeded=self.seeded, maxcontent=self.maxcontent)
            else: 
                disp=Remove(prob=1,adsorbate=self.adsorbate,adjust_cm=self.adjust_cm,atm=self.atm, doublesurf=self.doublesurf, seeded=self.seeded)
            tmp=disp.displace(tmp, seed)
        else:
            if self.ads:    ## cannot be done in init bc it has to be updated... find a way
                adsidx=[atom.index for atom in tmp if atom.tag==self.adsorbate]    ### convert adsorbate by tag to adsorbate by index
                #print 'ads = ', (adsidx[0],adsidx[-1]+1)
                disp=DI(self.stepwidth, numdelocmodes=self.numdelmodes, constrain=self.constrain, adjust_cm=self.adjust_cm, adsorbate=(adsidx[0],adsidx[-1]+1), periodic=self.periodic, cell_scale=self.cell_scale, doublesurf=self.doublesurf, seeded = self.seeded, dense=self.dense, weighted=self.weighted, thresholds=self.thresholds, per_version=self.expand, expand=self.expand)
                #print 'displacing by '+str(self.stepwidth)+', ads = '+str(adsidx[0])+','+str(adsidx[-1]+1)
            else:
                disp=DI(self.stepwidth,numdelocmodes=self.numdelmodes,constrain=self.constrain, adjust_cm=self.adjust_cm, adsorbate=None, dense=self.dense, weighted=self.weighted, thresholds=self.thresholds, seeded = self.seeded)
                #print 'displacing by '+str(self.stepwidth)
            tmp=disp.displace(tmp, seed=0)
        if self.adjust_cm:
            cm = tmp.get_center_of_mass()
            tmp.translate(cmt - cm)   
        return tmp
    
    def print_params(self):
        if self.ads:
            ads=', %d: is displaced'%(self.adsorbate)
        else:
            ads=''
        return '%s: probability=%f%s'%(self.__class__.__name__,self.prob,ads)

