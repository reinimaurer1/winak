# winak.screening.ultimatescreener
#
#    winak - python package for structure search and more in curvilinear coordinates
#    Copyright (C) 2016  Reinhard J. Maurer and Konstantin Krautgasser 
#    
#    This file is part of winak 
#        
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>#

import numpy as np
from datetime import datetime
from ase.io.trajectory import Trajectory
from ase.visualize import view ##CP for debugging purposes
from collections import Counter
from winak.screening.composition import Stoichiometry
try:
    from ase.build.tools import sort
except:
    from ase.utils.geometry import sort
import os 


class UltimateScreener:
    """UltimateScreener

    by Konstantin Krautgasser, November 2015
    """

    def __init__(self, atoms,
                 EnergyEvaluator,
                 Displacer,
                 Criterion,
                 trajectory='minima.traj',
                 logfile='tt.log',
                 patience=10, ## number of tries for fallback
                 savesnaps=True,   ## or interval (TODO)
                 savetrials=True):
        self.atoms=atoms
        self.logfile=logfile
        self.eneval=EnergyEvaluator
        self.displacer=Displacer
        self.crit=Criterion
        self.trajall=Trajectory(trajectory,'w')
        ### set initial trajectory with composition
        self.comp=Stoichiometry()
        self.comp.get(atoms)
        self.traj=self.comp.make_traj()
        ###
        self.startT = datetime.now()
        self.log('STARTING Screening at '+self.startT.strftime('%Y-%m-%d %H:%M:%S')+' KK 2015')
        self.log('Using the following Parameters and Classes:')
        self.log('EnergyEvaluator - '+self.eneval.print_params())
        self.log('Displacer - '+self.displacer.print_params())
        self.log('Criterion - '+self.crit.print_params())
        self.savesnaps=savesnaps ## if True, store relax snapshots in folder 'snaps'
        self.savetrials=savetrials ## if True, store all trial moves in folder 'trial'
        self.patience=patience

    def run(self, steps):
        """Screen for defined number of steps."""
        t_acc = Trajectory('accepted.traj','w')
        #print self.atoms.copy()
        tmp = self.eneval.get_energy(self.atoms.copy())
        if tmp is None:
            self.log('Initial Energy Evaluation Failed. Please check your calculator!')
        else:
            #print 'tmp', tmp[1]
            tmp[0].info={'accepted':True}
            t_acc.write(tmp[0], energy=tmp[1])
            self.current=tmp[0];self.Emin=tmp[1]
            self.crit.evaluate(tmp[0].copy(),tmp[1])
            self.traj.write(self.current)
            self.trajall.write(self.current,energy=tmp[1])
            self.log('Initial Energy Evaluation done. Note that this is structure 0 in your trajectory. Energy = %f, Stoichiometry = %s' %(tmp[1],self.comp.stoich))
        self.fallbackatoms=self.current.copy()
        self.fallbackstep=-1
        ## CP adjust COM
        '''
        cm = self.current.get_center_of_mass()
        try:
            cell = self.current.get_cell()
            cmt = np.array([cell[0][0], cell[1][1], cell[2][2]])/2
        except:
            cmt = self.current.get_center_of_mass()
        self.current.translate(cmt - cm)
        '''

        if self.savetrials:
            os.system('mkdir -p trial') 
        
        if self.savesnaps:
            os.system('mkdir -p snaps') 
	
        for step in range(steps):    
            """I strictly use copies here, so nothing can be overwritten in a subclass.
            A step is tried 10 times, if it keeps failing, the structure is reset.
            If it still fails, abort, since something is clearly wrong."""
            tmp=None
            tries=0
            reset=False  
            failed=False          
            while tmp is None:
                try:
                    tmp=self.displacer.displace(self.current.copy(),seed=step+tries)
                except:
                    tmp=None
                tries+=1
                if tmp is None:
                    self.log('Error while displacing, retrying')
                else:
                    if self.savetrials:
                        tmp.write('trial/trial'+str(step+1).zfill(3)+'.xyz')
                    tmp=self.eneval.get_energy(tmp.copy())#,self.current.copy())
                    if tmp is None:
                        self.log('Error while evaluating energy, retrying')
                    else:
                        ### CP successful step: set fallback NOW and to last successful (current), not to last accepted
                        ### which I would prefer, but runs into trouble if DI displacer produces a dissociated structure which is accepted
                        self.fallbackatoms=self.current.copy()
                        #view(self.fallbackatoms) ##CP maybe make trajectories out of this for debugging, to monitor the fallback
                        self.fallbackstep=step
                if tries>self.patience and not reset:
                    self.log('Repeated Error during current step, rolling back to step %d' %self.fallbackstep)
                    #view(self.current)
                    self.current=self.fallbackatoms.copy()
                    #view(self.current)
                    tries=0
                    reset=True
                if tries>self.patience and reset:
                    failed=True
                    break
                if self.savesnaps:
                    os.system('mv snaps.traj snaps/snaps'+str(step+1).zfill(3)+'.traj')
            if failed:
                self.log('ABORTING. COULD NOT PERFORM STEP.')
                break

            ### cleanup after castep
            #try:
            if os.path.exists('CASTEP'):
                os.system('rm CASTEP/castep*')
                print 'cleanup after castep...'
            #except:
            #    pass
            
            """change trajectory if stoichiometry has changed"""
            ## no longer needed with new ase apparently
            comp=Stoichiometry()
            oldtraj=self.traj
            if comp.has_changed(tmp[0],self.current):
                newtraj = comp.make_traj()
                self.traj=newtraj
            
            """setting backup"""
            accepted=self.crit.evaluate(tmp[0].copy(),tmp[1])
            if accepted:
                tmp[0].info={'accepted':True}
                t_acc.write(tmp[0], energy=tmp[1])
            else:
                tmp[0].info={'accepted':False}
            self.traj.write(tmp[0])
            self.trajall.write(tmp[0],energy=tmp[1])
            
            acc='not'
            if accepted:       
                self.current=tmp[0].copy()
                acc=''
                #self.fallbackatoms=tmp[0].copy()
                #self.fallbackstep=step
            else:
                self.traj=oldtraj
            self.log('%s - step %d done, %s accepted, Energy = %f, Stoichiometry = %s '%(datetime.now().strftime('%Y-%m-%d %H:%M:%S'),step+1,acc,tmp[1], comp.stoich))
        self.endT = datetime.now()
        self.log('ENDING Screening at '+self.endT.strftime('%Y-%m-%d %H:%M:%S'))
        self.log('Time elapsed: '+str(self.endT-self.startT))
            
       
    def log(self, msg):
        if self.logfile is not None:
            with open(self.logfile,'a') as fn:
                fn.write(msg+'\n')
                fn.flush()
