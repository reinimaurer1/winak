from distutils.core import setup
from distutils.extension import Extension

try:
    from Cython.Distutils import build_ext
    use_cython = True
except:
    use_cython = False
    
if use_cython:
# the following provides: __version__, __revision__, __all__
    import numpy

    execfile('__init__.py')

    package_name = 'winak'

    packages = [ package_name ]
    for package in __all__:
        packages.append(package_name + '.' + package)

    ext_modules = [
        Extension("curvilinear.cICTools", 
                    ["curvilinear/cICTools.pyx",], 
                    libraries = ['blas',],
                    #include_dirs = [numpy.get_include(), '/path/to/blas/include/'],
                    include_dirs = [numpy.get_include(), '/home/chiarapan/.conda/envs/py2/include/'],
                    ),
    ]

    setup(
        package_dir = { package_name: '.' },
        cmdclass = {'build_ext': build_ext},
        packages = packages,
        ext_package = package_name,
        ext_modules = ext_modules,
    )

else:
    print('Please install cython before running this file!')
